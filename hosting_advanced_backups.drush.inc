<?php

function drush_hosting_advanced_backups_pre_provision_backup() {
  $backup_file = drush_get_option('backup_file');

  drush_log('Backup file: ' .   $backup_file);

  if (substr($backup_file, -3) == '.gz') {
    // Truncate the '.gz' so that the backup isn't gzipped
    $new_file_name = substr($backup_file, 0, strlen($backup_file) - 3);

    drush_log('New name: ' . $new_file_name);

    drush_set_option('backup_file', $new_file_name);
  }
}
